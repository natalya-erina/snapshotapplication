﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnapshotApplication
{
    public partial class MainForm : Form
    {
        private MainThread maintThread;

        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
            maintThread = new MainThread(textBoxMain, labelCountHeaps, labelCountBlocks);
            maintThread.Start();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (maintThread.CurrentThread.IsAlive)
                maintThread.CurrentThread.Abort();
        }
    }
}
