﻿namespace SnapshotApplication
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonFind = new System.Windows.Forms.Button();
            this.textBoxMain = new System.Windows.Forms.TextBox();
            this.labelCountHeaps = new System.Windows.Forms.Label();
            this.labelCountBlocks = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonFind
            // 
            this.buttonFind.Location = new System.Drawing.Point(15, 28);
            this.buttonFind.Name = "buttonFind";
            this.buttonFind.Size = new System.Drawing.Size(75, 23);
            this.buttonFind.TabIndex = 0;
            this.buttonFind.Text = "Start";
            this.buttonFind.UseVisualStyleBackColor = true;
            this.buttonFind.Click += new System.EventHandler(this.buttonFind_Click);
            // 
            // textBoxMain
            // 
            this.textBoxMain.Location = new System.Drawing.Point(12, 121);
            this.textBoxMain.Multiline = true;
            this.textBoxMain.Name = "textBoxMain";
            this.textBoxMain.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxMain.Size = new System.Drawing.Size(349, 302);
            this.textBoxMain.TabIndex = 1;
            // 
            // labelCountHeaps
            // 
            this.labelCountHeaps.AutoSize = true;
            this.labelCountHeaps.Location = new System.Drawing.Point(12, 72);
            this.labelCountHeaps.Name = "labelCountHeaps";
            this.labelCountHeaps.Size = new System.Drawing.Size(269, 13);
            this.labelCountHeaps.TabIndex = 2;
            this.labelCountHeaps.Text = "Процессов с максимальным количеством блоков: ";
            // 
            // labelCountBlocks
            // 
            this.labelCountBlocks.AutoSize = true;
            this.labelCountBlocks.Location = new System.Drawing.Point(12, 96);
            this.labelCountBlocks.Name = "labelCountBlocks";
            this.labelCountBlocks.Size = new System.Drawing.Size(163, 13);
            this.labelCountBlocks.TabIndex = 3;
            this.labelCountBlocks.Text = "Количество блоков в каждом: ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 435);
            this.Controls.Add(this.labelCountBlocks);
            this.Controls.Add(this.labelCountHeaps);
            this.Controls.Add(this.textBoxMain);
            this.Controls.Add(this.buttonFind);
            this.Name = "MainForm";
            this.Text = "Процессы";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonFind;
        private System.Windows.Forms.TextBox textBoxMain;
        private System.Windows.Forms.Label labelCountHeaps;
        private System.Windows.Forms.Label labelCountBlocks;
    }
}

