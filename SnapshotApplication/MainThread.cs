﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnapshotApplication
{
    class MainThread
    {
        private Thread thread;
        private TextBox textBox;
        private Label labelCountHeaps;
        private Label labelCountBlocks;
        private List<PROCESSENTRY32> processes;

        public Thread CurrentThread { get { return thread; } }

        public MainThread(TextBox textBox, Label labelHeaps, Label labelBlocks)
        {
            this.textBox = textBox;
            this.labelCountBlocks = labelBlocks;
            this.labelCountHeaps = labelHeaps;
            thread = new Thread(new ThreadStart(DoWork));
        }

        private void DoWork()
        {
            Helper.GetMaxProcesses(); 
        }

        private string GetMaxProcessesAsString()
        {
            StringBuilder result = new StringBuilder();
            processes = Helper.maxProcesses;
            foreach (PROCESSENTRY32 pr in processes)
            {
                result.Append(pr).Append(Environment.NewLine)
                    .Append("-------------------------------------------------------------")
                    .Append(Environment.NewLine);
            }
            return result.ToString();
        }

        private void PrintToTextBox()
        {
            string result = GetMaxProcessesAsString();
            if (textBox.InvokeRequired)
                textBox.Invoke(new Action<string>((s) => textBox.AppendText(s)), result);
            else
                textBox.AppendText(result);
        }

        private void PrintToLables()
        {
            int resultHeaps = processes.Count;
            if (labelCountHeaps.InvokeRequired)
                labelCountHeaps.Invoke(new Action<int>((s) => labelCountHeaps.Text += s), resultHeaps);
            else
                labelCountHeaps.Text += resultHeaps;

            int resultBlocks = Helper.maxCount;
            if (labelCountBlocks.InvokeRequired)
                labelCountBlocks.Invoke(new Action<int>((s) => labelCountBlocks.Text += s), resultBlocks);
            else
                labelCountBlocks.Text += resultBlocks;
        }

        public void Start()
        {
            thread.Start();
            thread.Join();
    
            PrintToTextBox();
            PrintToLables();
        }
    }
}
